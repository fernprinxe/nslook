# nslookup batch

[![nalook](https://img.shields.io/badge/license-BSD3--clause%20license-brightgreen)](https://en.wikipedia.org/wiki/BSD_licenses#3-clause_license_(%22BSD_License_2.0%22,_%22Revised_BSD_License%22,_%22New_BSD_License%22,_or_%22Modified_BSD_License%22))

## usage

### edit

first, you need to replace the root domain in the list of domain names.  
replace `.local` with `.com` or any main domain name you need.

### log

the script turns off log output by default.  
if you need logs, deleting `@echo off` is a good option.