@echo off
for /f "delims=" %%a in (domain.list) do (
for /f "skip=1 tokens=2 delims=, " %%b in ('nslookup %%a 2^>nul^|findstr "[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*.[0-9]"') do (call echo %%a#%%b>>result.txt))
type result.txt
pause